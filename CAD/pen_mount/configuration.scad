// Settings
$fn = 16;

pen_diameter=8;
pen_depth=2*pen_diameter;
pen_round_radius=8;
pen_servo_height=25;
pen_servo_width=23;
pen_servo_depth=13;
pen_servo_hole_offset=2.5;
pen_servo_hole_diameter=2.5;
pen_servo_hole_height=5;
pen_hook_size=6;

x_carriage_length = 86; // Length of carriage along x-axis
x_carriage_hole_height = 30;
x_carriage_hole_diameter = 3.5;

pen_mount_z = 61;
// x_smooth_rod = 8; // Diameter of smooth rods
// x_rod_gap = 45; // Distance between rods

extruder_idler_spring_int_diameter = 5;