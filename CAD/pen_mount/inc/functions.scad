module right_angle_prism(x, y, z) { // 82
	scale([x, y, z]) polyhedron(
		points = [[0, 0, 0], [0, 0, 1], [1, 0, 0], // Front 3 points
		          [0, 1, 0], [0, 1, 1], [1, 1, 0]], // Rear 3 points
		faces = [[0, 1, 2], [4, 3, 5], // Triangle ends (front and rear)
		         [0, 2, 3], [5, 3, 2], // Horizontal side (z = 0)
		         [1, 0, 3], [3, 4, 1], // Vertical side (x = 0)
		         [2, 1, 4], [4, 5, 2]] // Hypotenuse side
	);
}

module board(width, length) {
	color("SandyBrown")	cube([width, length, board_thickness]);
}

module rod(diameter, length) {
	color("LightGrey") cylinder(h = length, d = diameter);
}

module washer(internal, external, thickness) {
	color("LightGrey") difference() {
		cylinder(h = thickness, d = external, center = true);
		cylinder(h = 2*thickness, d = internal, center = true);
	}
}

module nut(internal, external, thickness) {
	color("LightGrey") difference() {
		cylinder(h = thickness, d = external, center = true, $fn = 6);
		cylinder(h = 2*thickness, d = internal, center = true);
	}
}

module stepper() {
	module stepper_bolt() {
		translate([stepper_side/2 - stepper_bolt_padding, stepper_side/2 - stepper_bolt_padding, stepper_depth/2 - stepper_bolt_depth]) rod(stepper_bolt_diameter, 2*stepper_bolt_depth);
	}
	
	color("LightGrey") {
		union() {
			// Body
			difference() {
				translate([-stepper_side/2, -stepper_side/2, -stepper_depth/2]) cube([stepper_side, stepper_side, stepper_depth]);
				stepper_bolt();
				mirror([1, 0, 0]) stepper_bolt();
				mirror([0, 1, 0]) stepper_bolt();
				mirror([1, 0, 0]) mirror([0, 1, 0]) stepper_bolt();
			}

			// Shaft
			translate([0, 0, stepper_depth/2]) rod(stepper_shaft_diameter, stepper_shaft_length);
			
			// Shaft relief
			translate([0, 0, stepper_depth/2]) rod(stepper_shaft_relief_diameter, stepper_shaft_relief_depth);
		}
	}
}

module bearing_624() {
	color("LightGrey") {
		difference() {
			cylinder(d=624_ext_diameter, h=624_thickness);
			translate([0, 0, -624_thickness]) cylinder(d=624_int_diameter, h=3*624_thickness);
		}
	}
} // 144

module woodscrew_cutout() {
	union() {
		translate([0, 0, -100]) cylinder(d=7, h=100);
		cylinder(d1=7, d2=3.5, h=3);
		translate([0, 0, 3]) cylinder(d=3.5, h=100);
	}
}