module pen_mount_body() {
	color("White") {
		difference() {
			union() {
				// Top
				translate([-x_carriage_length/2 + pen_round_radius/2, -pen_depth, pen_mount_z - pen_round_radius]) cube([x_carriage_length - pen_round_radius, pen_depth, pen_round_radius]);
				// Bottom
				translate([-x_carriage_length/2 + pen_round_radius/2, -pen_depth, 0]) cube([x_carriage_length - pen_round_radius, pen_depth, pen_round_radius]);
				// Left
				translate([-x_carriage_length/2, -pen_depth, pen_round_radius/2]) cube([pen_round_radius, pen_depth, pen_mount_z - pen_round_radius]);
				// Right
				translate([x_carriage_length/2 - pen_round_radius, -pen_depth, pen_round_radius/2]) cube([pen_round_radius, pen_depth, pen_mount_z - pen_round_radius]);

				// Corners
				translate([-x_carriage_length/2 + pen_round_radius/2, 0, pen_round_radius/2]) rotate([90, 0, 0]) cylinder(d=pen_round_radius, h=pen_depth);
				translate([x_carriage_length/2 - pen_round_radius/2, 0, pen_round_radius/2]) rotate([90, 0, 0]) cylinder(d=pen_round_radius, h=pen_depth);
				translate([-x_carriage_length/2 + pen_round_radius/2, 0,  pen_mount_z - pen_round_radius/2]) rotate([90, 0, 0]) cylinder(d=pen_round_radius, h=pen_depth);
				translate([x_carriage_length/2 - pen_round_radius/2, 0,  pen_mount_z - pen_round_radius/2]) rotate([90, 0, 0]) cylinder(d=pen_round_radius, h=pen_depth);

				// Spring mounts
				translate([8, -pen_depth/2, pen_mount_z - pen_round_radius - 2]) cylinder(d=extruder_idler_spring_int_diameter - 0.5, h=2);
				translate([-8, -pen_depth/2, pen_mount_z - pen_round_radius - 2]) cylinder(d=extruder_idler_spring_int_diameter - 0.5, h=2);

				// Servo mounts
				translate([pen_servo_hole_offset, -pen_depth, pen_mount_z - pen_round_radius]) cube([3*pen_servo_hole_offset, pen_servo_hole_height, pen_servo_depth + pen_round_radius]);
				translate([4*pen_servo_hole_offset + pen_servo_width, -pen_depth, pen_mount_z - pen_round_radius]) cube([3*pen_servo_hole_offset, pen_servo_hole_height, pen_servo_depth + pen_round_radius]);
			}
			// Pen axis
			translate([0, -pen_depth/2, -pen_mount_z/2]) cylinder(d=pen_diameter+0.5, h=2*pen_mount_z);

			// Carriage mounts
			translate([-x_carriage_length/2 + x_carriage_hole_diameter - 0.5, pen_depth, x_carriage_hole_height]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=3*pen_depth);
			translate([x_carriage_length/2 - x_carriage_hole_diameter + 0.5, pen_depth, x_carriage_hole_height]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=3*pen_depth);

			// Servo mounts
			translate([2*pen_servo_hole_offset + pen_servo_hole_offset, -pen_depth + 2*pen_servo_hole_height, pen_mount_z + pen_servo_depth/2]) rotate([90, 0, 0]) cylinder(d=pen_servo_hole_diameter, h=3*pen_servo_hole_height);
			translate([4*pen_servo_hole_offset + pen_servo_width + pen_servo_hole_offset, -pen_depth + 2*pen_servo_hole_height, pen_mount_z + pen_servo_depth/2]) rotate([90, 0, 0]) cylinder(d=pen_servo_hole_diameter, h=3*pen_servo_hole_height);
		}
	}
}

module pen_stopper() {
	color("White") {
		difference() {
			union() {
				// Body
				translate([-2*pen_diameter, -pen_depth, pen_round_radius + 1]) cube([4*pen_diameter, pen_depth - 1, 1.5*pen_round_radius]);

				// Spring mounts
				translate([8, -pen_depth/2, 2.5*pen_round_radius + 1]) cylinder(d=extruder_idler_spring_int_diameter - 0.5, h=2);
				translate([-8, -pen_depth/2, 2.5*pen_round_radius + 1]) cylinder(d=extruder_idler_spring_int_diameter - 0.5, h=2);

				// Servo hook
				translate([-pen_hook_size/2, -pen_depth - 2*pen_hook_size, pen_round_radius + 1]) cube([pen_hook_size, 2*pen_hook_size, pen_hook_size]);
				translate([-1.5*pen_hook_size, -pen_depth - 2*pen_hook_size, pen_round_radius + 1]) cube([3*pen_hook_size, pen_hook_size, pen_hook_size]);
			}
			// Pen axis
			translate([0, -pen_depth/2, pen_round_radius]) cylinder(d=pen_diameter+0.1, h=3*pen_round_radius);
		}
	}
}

module pen() {
	union() {
		cylinder(d=pen_diameter, h=150);
		translate([0, 0, -25]) cylinder(d1=0, d2=pen_diameter, h=25);
	}
}

module pen_mount(pen=1) {
	translate([0, -z_padding - x_padding - x_smooth_rod - 0.5, -pen_mount_z/2]) {
		// Body
		pen_mount_body();

		// Pen
		if(pen == 1) {
			translate([0, -pen_depth/2, -20]) pen();
		}

		// Pen stopper
		pen_stopper();
	}
}
