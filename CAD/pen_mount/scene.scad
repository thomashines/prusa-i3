include <configuration.scad>
include <inc/functions.scad>

include <pen-mount.scad>

translate([0, 0, pen_depth]) rotate([-90, 180, 0]) pen_mount_body();
translate([0, -10, -pen_round_radius - 1]) pen_stopper();