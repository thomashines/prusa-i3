$fn = 16;

difference() {
    union() {
        cube([10, 40, 4]);
        translate([0, -4, 0]) cube([10, 4, 10]);
    }
    translate([5, 22, -1]) cylinder(r=1.75, h=6);
    translate([5, 22, -1]) cylinder(r=3.25, h=3, $fn=6);
}