// Settings
//$fn = 8;
$fn = 32;
use_fillets = 1;

// Vitamins

// Stepper
stepper_side = 42;
stepper_depth = 39;

stepper_bolt_padding = 5.5;
stepper_bolt_depth = 10;
stepper_bolt_diameter = 3.5;

stepper_shaft_diameter = 5;
stepper_shaft_length = 20;

stepper_shaft_relief_diameter = 22;
stepper_shaft_relief_depth = 2;

// Shaft coupler
coupler_h = 25;
coupler_h_buffer = 33;
coupler_d = 19;

// Hot end
hotend_mount_diameter = 13;
hotend_mount_length = 68;
hotend_mount_width = 25;
hotend_mount_depth = 4;
hotend_mount_hole_diameter = 3.5;
hotend_mount_hole_spacing = 38 + hotend_mount_hole_diameter;
jhead_diameter = hotend_mount_diameter;
jhead_mount_spacing = hotend_mount_hole_spacing;
jhead_mount_diameter = hotend_mount_hole_diameter;
v6_int_diameter = 2;
v6_diameter_1 = 16;
v6_length_1 = 4;
v6_diameter_2 = 12;
v6_length_2 = 6;
v6_diameter_3 = 16;
v6_length_3 = 7;
v6_diameter_4 = 24;
v6_length_4 = 26;

// 608
608_ext_diameter = 22;
608_int_diameter = 8;
608_thickness = 7.5;

// 624
624_ext_diameter = 13;
624_int_diameter = 4;
624_thickness = 5;

// lm8uu
lm8uu_ext_diameter = 15;
lm8uu_int_diameter = 8.1;
lm8uu_length = 24;

// Timing belt
belt_width = 6.25;
belt_depth = 2.5;
belt_pitch = 2.5;
belt_gear_diameter = 12;

// Fasteners
m3_diameter = 3.5;
m3_cap_diameter = 8.0;
m3_nut_diameter = 7.0;
m3_buffer = 5;
woodscrew_buffer = 5;

// Printed plastic
ideal_thickness = 5;

// Frame
board_thickness = 9; // Thickness of frame boards
frame_depth = 100; // Depth of frame pieces (sort of arbitrary)
frame_x = 500; // Length of shortest frame piece along the x-axis
frame_z = 600; // Length of shortest frame piece along the z-axis
frame_bottom_height = 18;

// Spool holder
spool_style = 0; // 0: rod; 1: board
spool_rod_diameter = 14;
spool_rod_length = 360;
spool_board_width = 25;
spool_board_length = 250;

// Nozzle position
position_x = 0;
position_y = 0;
position_z = 0;

// x-axis
x_length = 500; // Length of smooth rods
x_smooth_rod = 8; // Diameter of smooth rods
x_padding = 15; // Distance between x and z axis rods
x_rod_gap = 45; // Distance between rods
x_rod_gap_flat = 65; // Distance between rods for the flat version
x_carriage_length = 86; // Length of carriage along x-axis
x_carriage_hole_height = 30;
x_carriage_hole_diameter = 3.5;
x_carriage_hole_spacing_1 = 30;
x_carriage_hole_spacing_2 = 20;
x_stepper_offset_x = 8;
x_stepper_offset_z = 4;

// y-axis
y_length = 460; // Length of smooth rods
y_width = 160; // Length of x-axis threaded rods on the y-axis
y_difference = 15; // Length of x-axis threaded rods on the y-axis extending beyond the corners
y_threaded_rod = 8; // Diameter of threaded rods
y_smooth_rod = 8; // Diameter of smooth rods (should be less than or equal to the threaded diameter)
y_washer_thickness = 1; // Thickness of washers on threaded rod
y_washer_external_diameter = 17; // External diameter of washers on threaded rod
y_nut_thickness = 6; // Thickness of nuts on threaded rod
y_nut_external_diameter = 15; // Greatest external diagonal of nuts on threaded rod

// z-axis
z_length = 460; // Length of smooth rods
z_padding = x_rod_gap_flat/2 + x_smooth_rod + 10; // Distance between z axis rods and frame
z_rod_start_height = 60; // Distance between z axis rods and frame
z_smooth_rod = 8; // Diameter of smooth rods
z_smooth_rod_h = 460; // Length of smooth rods
z_threaded_rod = 10; // Diameter of threaded rods
z_threaded_rod_h = 500; // Length of threaded rods
z_idler_height = 20;
z_smooth_rod_brace_x = 10;
z_nut_diameter = 15;
z_nut_depth = 6.5;
z_leadnut_diameter = 22;
z_leadnut_depth = 15;
z_spring_length = 10;
z_shaft_couple_d = max([z_threaded_rod, stepper_shaft_diameter]) + 2 * ideal_thickness;
z_shaft_couple_h = stepper_shaft_length + 16;
z_driven_z = coupler_h_buffer + 2 * m3_buffer + ideal_thickness;

// Extruder
filament_diameter = 1.75;
filament_offset_y = 18;
filament_gear_diameter = 6.6;
cut_away_right = 20;
cut_away_left = 35;
cut_away_center_x = 40;
cut_away_center_z = 30;
cut_away_center_lower_x = x_carriage_length + 2;
cut_away_center_lower_z = x_carriage_hole_height - x_carriage_hole_diameter - ideal_thickness + 1;
extruder_idler_diameter = stepper_bolt_diameter + ideal_thickness;
extruder_idler_thickness = 624_thickness + 2*ideal_thickness; // 81
extruder_stepper_mount_bolt_ext = 8;
// Extruder idler spring
extruder_idler_spring_int_diameter = 5;
extruder_idler_spring_extra_compression = 5; // 15 final
filament_stepper_gap = 12;
extruder_bearing_position = 608_ext_diameter/2 + filament_gear_diameter/2;

// Pen mount
pen_diameter=8;
pen_depth=2*pen_diameter;
pen_round_radius=8;
pen_servo_height=25;
pen_servo_width=23;
pen_servo_depth=13;
pen_servo_hole_offset=2.5;
pen_servo_hole_diameter=2.5;
pen_servo_hole_height=5;
pen_hook_size=6;
