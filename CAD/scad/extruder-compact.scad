module extruder_compact_hotend_plate() {
    translate([-hotend_mount_width/2, -hotend_mount_length/2, -hotend_mount_depth]) color("LightGrey") {
        difference() {
            cube([hotend_mount_width, hotend_mount_length, hotend_mount_depth]);
            translate([hotend_mount_width/2, hotend_mount_length/2, -1]) cylinder(d=hotend_mount_diameter, h=hotend_mount_depth+2);
            translate([-1, hotend_mount_length/2 - hotend_mount_diameter/2, -1]) cube([hotend_mount_width/2 + 1, hotend_mount_diameter, hotend_mount_depth + 2]);
            translate([hotend_mount_width/2, hotend_mount_length/2 + hotend_mount_hole_spacing/2, -1]) cylinder(d=hotend_mount_hole_diameter, h=hotend_mount_depth+2);
            translate([hotend_mount_width/2, hotend_mount_length/2 - hotend_mount_hole_spacing/2, -1]) cylinder(d=hotend_mount_hole_diameter, h=hotend_mount_depth+2);
        }
    }
}

module extruder_compact_hotend() {
    color("LightGrey") mirror([0, 0, 1]) difference() {
        union() {
            cylinder(d=v6_diameter_1, h=v6_length_1);
            translate([0, 0, v6_length_1]) {
                cylinder(d=v6_diameter_2, h=v6_length_2);
                translate([0, 0, v6_length_2]) {
                    cylinder(d=v6_diameter_3, h=v6_length_3);
                    translate([0, 0, v6_length_3]) cylinder(d=v6_diameter_4, h=v6_length_4);
                }
            }
        }
        translate([0, 0, -1])
        cylinder(d=v6_int_diameter, h=v6_length_1 + v6_length_2 + v6_length_3 + v6_length_4 + 2);
    }
}

module extruder_compact_mount() {
    color("Green") mirror([1, 0, 0]) {
        difference() {
            union() {
                // Nut mount
                translate([0, -stepper_side/2, 0]) cube([2*filament_stepper_gap, 2*stepper_bolt_padding, stepper_side - 2*stepper_bolt_padding - 1]);
                // Filament guide block
                translate([0, -stepper_side/2, 0]) cube([2*filament_stepper_gap, stepper_side - 2*stepper_bolt_padding - 1, stepper_side/2]);
            }
            // Bearing cutout
            translate([-1, extruder_bearing_position + 1, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=608_ext_diameter + 2, h=2*filament_stepper_gap + 2);
            // Stepper relief cutout
            translate([-1, 0, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=stepper_shaft_relief_diameter + 2, h=2*filament_stepper_gap + 2);
            // Stepper bolt cutout
            translate([-1, stepper_bolt_padding - stepper_side/2, stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=2*filament_stepper_gap + 2);
            // Stepper mount
            translate([-1, -stepper_side/2 - 1, -1]) cube([m3_buffer + 1.5, 2*stepper_bolt_padding + 1.5, 2*stepper_bolt_padding + 1.5]);
            // Filament guide
            translate([filament_stepper_gap, (filament_gear_diameter + filament_diameter)/2, -1]) cylinder(d=4, h=stepper_side + 2);
            // Hotend mount plate cutout
            translate([filament_stepper_gap, (filament_gear_diameter + filament_diameter)/2 - hotend_mount_hole_spacing/2, stepper_side - 2*stepper_bolt_padding - 5]) rotate([0, 0, 180]) m3_nut_cutout(40, 20, 20);
        }
    }
}

module extruder_compact_idler() {
    color("Green") mirror([1, 0, 0]) {
        difference() {
            union() {
                // Pivot
                translate([0, stepper_side/2 - stepper_bolt_padding, stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=2*stepper_bolt_padding, h=2*filament_stepper_gap);
                // Vertical
                translate([0, stepper_side/2 - 2*stepper_bolt_padding, stepper_bolt_padding]) cube([2*filament_stepper_gap, 2*stepper_bolt_padding, stepper_side - 2*stepper_bolt_padding]);
                // Bearing
                translate([0, extruder_bearing_position + 1, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=608_ext_diameter, h=2*filament_stepper_gap);
                // Corner
                translate([0, stepper_side/2 - stepper_bolt_padding, stepper_side - stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=2*stepper_bolt_padding, h=2*filament_stepper_gap);
                // Horizontal
                translate([0, -stepper_side/2 - m3_buffer, stepper_side - 2*stepper_bolt_padding]) cube([2*filament_stepper_gap, stepper_side - stepper_bolt_padding + m3_buffer, 2*stepper_bolt_padding]);
            }
            // Stepper mount
            translate([-1, stepper_side/2 - stepper_bolt_padding, stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(r=sqrt(2)*stepper_bolt_padding + 1, h=m3_buffer + 1.5);
            // Stepper relief cutout
            translate([-1, 0, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=stepper_shaft_relief_diameter + 2, h=2*filament_stepper_gap + 2);
            // Stepper bolt cutout
            translate([-1, stepper_side/2 - stepper_bolt_padding, stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=2*filament_stepper_gap + 2);
            // Bearing shaft cutout
            translate([-1, extruder_bearing_position + 1, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=608_int_diameter, h=2*filament_stepper_gap + 2);
            // Bearing cutout
            translate([filament_stepper_gap - 608_thickness/2 - 1, extruder_bearing_position + 1, stepper_side/2]) rotate([0, 90, 0]) cylinder(d=608_ext_diameter + 2, h=608_thickness + 2);
            // Filament guide
            translate([filament_stepper_gap, (filament_gear_diameter + filament_diameter)/2, -1]) cylinder(d=6, h=stepper_side + 2);
            // Hotend mount plate cutout
            translate([filament_stepper_gap, (filament_gear_diameter + filament_diameter)/2 - hotend_mount_hole_spacing/2, -1]) {
                // Tightest
                cylinder(d=m3_diameter, h=stepper_side + 2);
                // Loosest
                translate([0, -4, 0]) cylinder(d=m3_diameter, h=stepper_side + 2);
                // Between
                translate([-m3_diameter/2, -4, 0]) cube([m3_diameter, 4, stepper_side + 2]);
            }
        }
    }
}

module extruder_compact() {
    translate([filament_stepper_gap, -(filament_gear_diameter + filament_diameter)/2, lm8uu_ext_diameter/2]) {
        extruder_compact_mount();
        extruder_compact_idler();
    }
    translate([0, 0, -lm8uu_ext_diameter/2]) {
        translate([0, 0, v6_length_1 + 0.5]) extruder_compact_hotend();
        extruder_compact_hotend_plate();
    }
}