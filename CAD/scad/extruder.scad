module extruder_body_mount_holes() { // 145
    translate([-x_carriage_length/2 + x_carriage_hole_diameter - 0.5, ideal_thickness, x_carriage_hole_height]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=ideal_thickness*3);
}

module extruder_body_stepper_mount() {
    translate([-stepper_side/2 + stepper_bolt_padding, ideal_thickness - 5, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=ideal_thickness*3 + 16 + extruder_idler_thickness/2 + 10);
}

module extruder_body_stepper_mount_ext() {
    translate([-stepper_side/2 + stepper_bolt_padding - ideal_thickness/2 - stepper_bolt_diameter/2, -ideal_thickness, x_rod_gap + 2*x_smooth_rod]) cube([stepper_side/2, ideal_thickness, stepper_side]);
}

module extruder_body() {
    color("Red") {
        difference() {
        //union() {
            union() {
                translate([-x_carriage_length/2, -ideal_thickness, 0]) cube([x_carriage_length, ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]);

                // Stepper
                translate([4.8, 0, 0]) {
                    extruder_body_stepper_mount_ext();
                    mirror([1, 0, 0]) extruder_body_stepper_mount_ext();

                    translate([stepper_side/2 - stepper_bolt_padding - extruder_idler_diameter/2, -filament_offset_y - extruder_idler_thickness/2, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding - extruder_idler_diameter/2]) {
                        // Idler offset
                        translate([-stepper_side + 2*stepper_bolt_padding,  extruder_idler_thickness + 3, 0]) cube([ideal_thickness + stepper_bolt_diameter, 15 - ideal_thickness - extruder_idler_thickness/2, ideal_thickness + stepper_bolt_diameter]);
                        
                        // Idler spring
                        translate([0, 0, 0]) cube([extruder_idler_diameter, filament_offset_y - ideal_thickness + extruder_idler_thickness/2, extruder_idler_diameter]);
                    }
                }

                // Hot end
                translate([-x_carriage_length/2, -3*ideal_thickness - jhead_diameter, x_carriage_hole_height - x_carriage_hole_diameter - ideal_thickness]) cube([x_carriage_length, jhead_diameter + 2*ideal_thickness, ideal_thickness]);

            }

            // Cutaways
            translate([x_carriage_length/2, -2*ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]) rotate([0, 45, 0]) translate([-cut_away_right/2, 0, -cut_away_right/2]) cube([cut_away_right, 3*ideal_thickness, cut_away_right]);
            translate([-x_carriage_length/2, -2*ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]) rotate([0, 45, 0]) translate([-cut_away_left/2, 0, -cut_away_left/2]) cube([cut_away_left, 3*ideal_thickness, cut_away_left]);
            translate([-cut_away_center_x/2, -2*ideal_thickness, x_rod_gap/2 + x_smooth_rod]) cube([cut_away_center_x, 3*ideal_thickness, cut_away_center_z]);
            translate([-cut_away_center_lower_x/2, -2*ideal_thickness, -1]) cube([cut_away_center_lower_x, 3*ideal_thickness, cut_away_center_lower_z]);
            translate([4.8, ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_side/2]) rotate([90, 0, 0]) cylinder(d=24, h=3*ideal_thickness);

            // Holes

            // Carriage mounts
            extruder_body_mount_holes();
            mirror([1, 0, 0]) extruder_body_mount_holes();

            // Stepper mounts
            translate([4.8, 0, 0]) {
                extruder_body_stepper_mount();
                mirror([1, 0, 0]) extruder_body_stepper_mount();
                translate([0, 0, stepper_side - 2*stepper_bolt_padding]) {
                    extruder_body_stepper_mount();
                    mirror([1, 0, 0]) extruder_body_stepper_mount();
                }
            }

            // Hot end mounts
            translate([0, -filament_offset_y, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_diameter, h=3*ideal_thickness);
            translate([-jhead_mount_spacing/2, -filament_offset_y, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_mount_diameter, h=3*ideal_thickness);
            translate([jhead_mount_spacing/2, -filament_offset_y, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_mount_diameter, h=3*ideal_thickness);
        }
    }
}

module extruder_stepper() {
    stepper();

    // Gear
    translate([0, 0, stepper_depth/2 + filament_offset_y - 10]) {
        difference() {
            union() {
                cylinder(d=12, h=5);
                difference() {
                    translate([0, 0, 5]) cylinder(d=8, h=8.5);
                    translate([0, 0, 10])
                        rotate_extrude(convexity = 10)
                            translate([4.8, 0, 0])
                                circle(d=3);
                }
            }
            translate([0, 0, -1]) cylinder(d=5.5, h=15.5);
        }
    }
}

module extruder_idler() {
    color("Red") {
        difference() {
        //union() {
            union() {
                // Pivot
                translate([0, 1, -stepper_side + 2*stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=extruder_idler_diameter, h=extruder_idler_thickness+extruder_stepper_mount_bolt_ext+1);
            
                // Idler arm
                translate([-extruder_idler_diameter/2, -extruder_idler_thickness, -stepper_side + 2*stepper_bolt_padding]) cube([extruder_idler_diameter, extruder_idler_thickness, stepper_side - stepper_bolt_padding]);
                
                // Idler shroud
                translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness - 624_thickness - 1, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+1, h=ideal_thickness-1);
                translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, 0, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+1, h=ideal_thickness-1);
            }

            // Bearing
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness + 1, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+5, h=624_thickness+2);
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, extruder_idler_thickness, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_int_diameter, h=3*extruder_idler_thickness);

            // Holes
            translate([0, extruder_idler_thickness, -stepper_side + 2*stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=stepper_bolt_diameter, h=3*extruder_idler_thickness);

            // Gear cut away
            translate([stepper_side/2 - stepper_bolt_padding, 1, stepper_bolt_padding - stepper_side/2]) rotate([90, 0, 0]) cylinder(d=16, h=2*extruder_idler_thickness);
        }
    }
}

module extruder() {
    translate([0, -z_padding - x_padding - x_smooth_rod - 0.5, -x_rod_gap/2 - x_smooth_rod]) {
        // Body
        extruder_body();

        // Stepper
        translate([4.8, stepper_depth/2, x_rod_gap + 2*x_smooth_rod + stepper_side/2]) rotate([90, 0, 0]) extruder_stepper();

        // Filament
        color("Blue") translate([0, -filament_offset_y, 20]) cylinder(d=3, h=100);

        // Idler
        translate([-stepper_side/2 + 4.8 + stepper_bolt_padding, -filament_offset_y + ideal_thickness + 624_thickness/2, x_rod_gap + 2*x_smooth_rod + stepper_side - stepper_bolt_padding]) {
            extruder_idler();
            
            // Idler bearing
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) bearing_624();
        }

    }
}
