module frame_board(length) {
    board(frame_depth, length);
}

module frame() {
    // x-axis
    // Top flat
    translate([frame_x/2 + board_thickness, 0, 0]) rotate([0, 0, 90]) frame_board(frame_x + 2*board_thickness);
    // Top vertical
    translate([frame_x/2, board_thickness, 0]) rotate([0, 90, 90]) frame_board(frame_x);
    // Bottom
    translate([frame_x/2, board_thickness, frame_bottom_height - board_thickness - frame_z]) rotate([0, 0, 90]) frame_board(frame_x);

    // z-axis
    // Left
    translate([frame_x/2, 0, -frame_z]) rotate([90, 0, 90]) frame_board(frame_z);
    translate([frame_x/2 - frame_depth, board_thickness, -frame_z]) rotate([90, 0, 0]) frame_board(frame_z);
    // Right
    translate([-frame_x/2 - board_thickness, 0, -frame_z]) rotate([90, 0, 90]) frame_board(frame_z);
    translate([-frame_x/2, board_thickness, -frame_z]) rotate([90, 0, 0]) frame_board(frame_z);
}