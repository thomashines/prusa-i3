module right_angle_prism(x, y, z) { // 82
    scale([x, y, z]) polyhedron(
        points = [[0, 0, 0], [0, 0, 1], [1, 0, 0], // Front 3 points
                  [0, 1, 0], [0, 1, 1], [1, 1, 0]], // Rear 3 points
        faces = [[0, 1, 2], [4, 3, 5], // Triangle ends (front and rear)
                 [0, 2, 3], [5, 3, 2], // Horizontal side (z = 0)
                 [1, 0, 3], [3, 4, 1], // Vertical side (x = 0)
                 [2, 1, 4], [4, 5, 2]] // Hypotenuse side
    );
}

module board(width, length) {
    color("SandyBrown")    cube([width, length, board_thickness]);
}

module rod(diameter, length) {
    color("LightGrey") cylinder(h = length, d = diameter);
}

module belt(length) {
    color("LightGrey") cube([length, belt_depth, belt_width]);
}

module washer(internal, external, thickness) {
    color("LightGrey") difference() {
        cylinder(h = thickness, d = external, center = true);
        cylinder(h = 2*thickness, d = internal, center = true);
    }
}

module nut(internal, external, thickness) {
    color("LightGrey") difference() {
        cylinder(h = thickness, d = external, center = true, $fn = 6);
        cylinder(h = 2*thickness, d = internal, center = true);
    }
}

module stepper() {
    module stepper_bolt() {
        translate([stepper_side/2 - stepper_bolt_padding, stepper_side/2 - stepper_bolt_padding, stepper_depth/2 - stepper_bolt_depth])
        rod(stepper_bolt_diameter, 2*stepper_bolt_depth);
    }

    color("LightGrey") {
        union() {
            // Body
            difference() {
                translate([-stepper_side/2, -stepper_side/2, -stepper_depth/2])
                cube([stepper_side, stepper_side, stepper_depth]);
                stepper_bolt();
                mirror([1, 0, 0]) stepper_bolt();
                mirror([0, 1, 0]) stepper_bolt();
                mirror([1, 0, 0]) mirror([0, 1, 0]) stepper_bolt();
            }

            // Shaft
            translate([0, 0, stepper_depth/2]) rod(stepper_shaft_diameter, stepper_shaft_length);

            // Shaft relief
            translate([0, 0, stepper_depth/2]) rod(stepper_shaft_relief_diameter, stepper_shaft_relief_depth);
        }
    }
}

module bearing_624() {
    color("LightGrey") {
        difference() {
            cylinder(d=624_ext_diameter, h=624_thickness);
            translate([0, 0, -624_thickness]) cylinder(d=624_int_diameter, h=3*624_thickness);
        }
    }
}

module bearing_lm8uu() {
    color("LightGrey") {
        rotate([0, 90, 0]) difference() {
            cylinder(d=lm8uu_ext_diameter, h=lm8uu_length);
            translate([0, 0, -1]) cylinder(d=lm8uu_int_diameter, h=lm8uu_length + 2);
        }
    }
}

module woodscrew_cutout() {
    union() {
        translate([0, 0, -100]) cylinder(d=7, h=100);
        cylinder(d1=7, d2=3.5, h=3);
        translate([0, 0, 3]) cylinder(d=3.5, h=100);
    }
}

module m3_bolt_cutout() {
    union() {
        translate([0, 0, -20]) cylinder(d=m3_diameter, h=20);
        translate([0, 0, 0]) cylinder(d=m3_cap_diameter, h=20);
    }
}

module m3_nut_cutout(left, right, out) {
    union() {
        translate([0, 0, -left]) cylinder(d=m3_diameter, h=left);
        translate([0, 0, 0]) cylinder(d=m3_diameter, h=right);
        translate([0, 0, -1.5]) cylinder(d=m3_nut_diameter, h=3, $fn=6);
        translate([-out, -cos(30)*m3_nut_diameter/2, -1.5]) cube([out, cos(30)*m3_nut_diameter, 3]);
    }
}
