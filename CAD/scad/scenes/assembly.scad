include <../configuration.scad>
include <../inc/functions.scad>

include <../frame.scad>
include <../spool-holder.scad>
include <../x-axis.scad>
include <../y-axis.scad>
include <../z-axis.scad>

include <../extruder.scad>
include <../extruder-compact.scad>
include <../pen-mount.scad>

// Frame
translate([0, 0, frame_z/2]) frame();

// Spool holder
if(spool_style == 0) {
    translate([frame_x/2 - frame_depth, frame_depth/2, frame_z/2 + board_thickness]) rotate([0, 0, -90]) spool_holder_rod();
} else if(spool_style == 1) {
    translate([frame_x/2 - frame_depth, frame_depth/2, frame_z/2 + board_thickness]) rotate([0, 0, -90]) spool_holder_board();
}

// x-axis
translate([0, 0, position_z]) x_axis_flat();

// y-axis
translate([0, -z_padding, -frame_z/2]) y_axis();

// z-axis
translate([0, 0, 0]) z_axis();

// Extruder
// translate([0, position_x, position_z]) extruder();
translate([position_x, -z_padding , position_z]) extruder_compact();

// Pen mount
//translate([0, position_x, position_z]) pen_mount();