include <../configuration.scad>
include <../inc/functions.scad>

include <../extruder.scad>

rotate([-90, 0, 0]) extruder_body();