include <../configuration.scad>
include <../inc/functions.scad>

include <../extruder-compact.scad>

rotate([0, 90, 0]) {
    extruder_compact_mount();
    translate([0, 10, 10]) extruder_compact_idler();
}