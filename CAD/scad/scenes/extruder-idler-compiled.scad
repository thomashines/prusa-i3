// Settings
$fn = 24;
use_fillets = 1;

// Vitamins

// Stepper
stepper_side = 42;
stepper_depth = 39;

stepper_bolt_padding = 5.5;
stepper_bolt_depth = 10;
stepper_bolt_diameter = 3.5;

stepper_shaft_diameter = 5;
stepper_shaft_length = 20;

// Hot end
jhead_diameter = 16;
jhead_mount_spacing = 50;
jhead_mount_diameter = 3.5;

// 624
624_ext_diameter = 13;
624_int_diameter = 4;
624_thickness = 5;

// Extruder idler spring
extruder_idler_spring_int_diameter = 4;
extruder_idler_spring_length = 20;

// Printed plastic
ideal_thickness = 5;

// Frame
board_thickness = 9; // Thickness of frame boards
frame_depth = 100; // Depth of frame pieces (sort of arbitrary)
frame_x = 500; // Length of shortest frame piece along the x-axis
frame_z = 600; // Length of shortest frame piece along the z-axis
frame_bottom_height = 18;

// Spool holder
spool_style = 0; // 0: rod; 1: board
spool_rod_diameter = 14;
spool_rod_length = 360;
spool_board_width = 25;
spool_board_length = 250;

// Nozzle position
position_x = 0;
position_y = 0;
position_z = 0;

// x-axis
x_length = 500; // Length of smooth rods
x_smooth_rod = 8; // Diameter of smooth rods
x_padding = 15; // Distance between x and z axis rods
x_rod_gap = 45; // Distance between rods
x_carriage_length = 86; // Length of carriage along x-axis
x_carriage_hole_height = 30;
x_carriage_hole_diameter = 3.5;
x_carriage_hole_spacing_1 = 30;
x_carriage_hole_spacing_2 = 20;

// y-axis
y_length = 460; // Length of smooth rods
y_width = 160; // Length of x-axis threaded rods on the y-axis
y_difference = 15; // Length of x-axis threaded rods on the y-axis extending beyond the corners
y_threaded_rod = 8; // Diameter of threaded rods
y_smooth_rod = 8; // Diameter of smooth rods (should be less than or equal to the threaded diameter)
y_washer_thickness = 1; // Thickness of washers on threaded rod
y_washer_external_diameter = 17; // External diameter of washers on threaded rod
y_nut_thickness = 6; // Thickness of nuts on threaded rod
y_nut_external_diameter = 15; // Greatest external diagonal of nuts on threaded rod

// z-axis
z_padding = 25; // Distance between z axis rods and frame

// Extruder
cut_away_right = 20;
extruder_idler_diameter = stepper_bolt_diameter + ideal_thickness;
extruder_idler_thickness = 624_thickness + 2*ideal_thickness; // 81

module right_angle_prism(x, y, z) { // 82
    scale([x, y, z]) polyhedron(
        points = [[0, 0, 0], [0, 0, 1], [1, 0, 0], // Front 3 points
                  [0, 1, 0], [0, 1, 1], [1, 1, 0]], // Rear 3 points
        faces = [[0, 1, 2], [3, 4, 5], // Triangle ends (front and rear)
                 [0, 2, 3], [5, 3, 2], // Horizontal side (z = 0)
                 [0, 1, 3], [4, 3, 1], // Vertical side (x = 0)
                 [1, 2, 4], [5, 4, 2]] // Hypotenuse side
        
    );
}

module board(width, length) {
    color("SandyBrown")    cube([width, length, board_thickness]);
}

module rod(diameter, length) {
    color("LightGrey") cylinder(h = length, d = diameter);
}

module washer(internal, external, thickness) {
    color("LightGrey") difference() {
        cylinder(h = thickness, d = external, center = true);
        cylinder(h = 2*thickness, d = internal, center = true);
    }
}

module nut(internal, external, thickness) {
    color("LightGrey") difference() {
        cylinder(h = thickness, d = external, center = true, $fn = 6);
        cylinder(h = 2*thickness, d = internal, center = true);
    }
}

module stepper() {
    module stepper_bolt() {
        translate([stepper_side/2 - stepper_bolt_padding, stepper_side/2 - stepper_bolt_padding, stepper_depth/2 - stepper_bolt_depth]) rod(stepper_bolt_diameter, 2*stepper_bolt_depth);
    }
    
    color("LightGrey") {
        union() {
            // Body
            difference() {
                translate([-stepper_side/2, -stepper_side/2, -stepper_depth/2]) cube([stepper_side, stepper_side, stepper_depth]);
                stepper_bolt();
                mirror([1, 0, 0]) stepper_bolt();
                mirror([0, 1, 0]) stepper_bolt();
                mirror([1, 0, 0]) mirror([0, 1, 0]) stepper_bolt();
            }

            // Shaft
            translate([0, 0, stepper_depth/2]) rod(stepper_shaft_diameter, stepper_shaft_length);
        }
    }
}

module bearing_624() {
    difference() {
        cylinder(d=624_ext_diameter, h=624_thickness);
        translate([0, 0, -624_thickness]) cylinder(d=624_int_diameter, h=3*624_thickness);
    }
} // 144

module extruder_body_mount_holes() { // 145
    translate([-x_carriage_length/2 + x_carriage_hole_diameter - 0.5, ideal_thickness, x_carriage_hole_height]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=ideal_thickness*3);
}

module extruder_body_stepper_mount() {
    translate([-stepper_side/2 + stepper_bolt_padding, ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=x_carriage_hole_diameter, h=ideal_thickness*3 + 16 + extruder_idler_thickness/2);
}

module extruder_body_stepper_mount_ext() {
    translate([-stepper_side/2 + stepper_bolt_padding - ideal_thickness/2 - stepper_bolt_diameter/2, -ideal_thickness, x_rod_gap + 2*x_smooth_rod]) cube([ideal_thickness + stepper_bolt_diameter, ideal_thickness, stepper_side]);
}

module extruder_body() {
    color("Red") {
        difference() {
        //union() {
            union() {
                translate([-x_carriage_length/2, -ideal_thickness, 0]) cube([x_carriage_length, ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]);

                // Stepper
                translate([4.8, 0, 0]) {
                    extruder_body_stepper_mount_ext();
                    mirror([1, 0, 0]) extruder_body_stepper_mount_ext();
                    

                    translate([stepper_side/2 - stepper_bolt_padding - extruder_idler_diameter/2, -16 - extruder_idler_thickness/2, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding - extruder_idler_diameter/2]) {
                        // Idler offset
                        translate([-stepper_side + 2*stepper_bolt_padding,  extruder_idler_thickness + 1, stepper_side - 2*stepper_bolt_padding]) cube([ideal_thickness + stepper_bolt_diameter, 15 - ideal_thickness - extruder_idler_thickness/2, ideal_thickness + stepper_bolt_diameter]);
                        
                        // Idler spring
                        cube([extruder_idler_diameter, 16 - ideal_thickness + extruder_idler_thickness/2, extruder_idler_diameter]);
                        translate([extruder_idler_diameter/2, extruder_idler_thickness/2, extruder_idler_diameter]) cylinder(d=extruder_idler_spring_int_diameter, h=1);
                    }
                }

                // Hot end
                translate([-x_carriage_length/2, -3*ideal_thickness - jhead_diameter, x_carriage_hole_height - x_carriage_hole_diameter - ideal_thickness]) cube([x_carriage_length, jhead_diameter + 2*ideal_thickness, ideal_thickness]);

            }

            // Cutaways
            translate([x_carriage_length/2, -2*ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]) rotate([0, 45, 0]) translate([-cut_away_right/2, 0, -cut_away_right/2]) cube([cut_away_right, 3*ideal_thickness, cut_away_right]);
            cut_away_left = 35;
            translate([-x_carriage_length/2, -2*ideal_thickness, x_rod_gap + 2*x_smooth_rod + stepper_bolt_padding + stepper_bolt_diameter/2 + ideal_thickness]) rotate([0, 45, 0]) translate([-cut_away_left/2, 0, -cut_away_left/2]) cube([cut_away_left, 3*ideal_thickness, cut_away_left]);
            cut_away_center_x = 40;
            cut_away_center_z = 30;
            translate([-cut_away_center_x/2, -2*ideal_thickness, x_rod_gap/2 + x_smooth_rod]) cube([cut_away_center_x, 3*ideal_thickness, cut_away_center_z]);
            cut_away_center_lower_x = 40;
            cut_away_center_lower_z = 15;
            translate([-cut_away_center_lower_x/2, -2*ideal_thickness, -1]) cube([cut_away_center_lower_x, 3*ideal_thickness, cut_away_center_lower_z]);

            // Holes

            // Carriage mounts
            extruder_body_mount_holes();
            mirror([1, 0, 0]) extruder_body_mount_holes();

            // Stepper mounts
            translate([4.8, 0, 0]) {
                extruder_body_stepper_mount();
                mirror([1, 0, 0]) extruder_body_stepper_mount();
                translate([0, 0, stepper_side - 2*stepper_bolt_padding]) {
                    extruder_body_stepper_mount();
                    mirror([1, 0, 0]) extruder_body_stepper_mount();
                }
            }

            // Hot end mounts
            translate([0, -16, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_diameter, h=3*ideal_thickness);
            translate([-jhead_mount_spacing/2, -16, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_mount_diameter, h=3*ideal_thickness);
            translate([jhead_mount_spacing/2, -16, x_carriage_hole_height - x_carriage_hole_diameter - 2*ideal_thickness]) cylinder(d=jhead_mount_diameter, h=3*ideal_thickness);
        }
    }
}

module extruder_stepper() {
    stepper();

    // Gear
    translate([0, 0, stepper_depth/2 + 6]) {
        difference() {
            union() {
                cylinder(d=12, h=5);
                difference() {
                    translate([0, 0, 5]) cylinder(d=8, h=8.5);
                    translate([0, 0, 10])
                        rotate_extrude(convexity = 10)
                            translate([4.8, 0, 0])
                                circle(d=3);
                }
            }
            translate([0, 0, -1]) cylinder(d=5.5, h=15.5);
        }
    }
}

module extruder_idler() {
    difference() {
    //union() {
        union() {
            // Pivot
            rotate([90, 0, 0]) cylinder(d=extruder_idler_diameter, h=extruder_idler_thickness);
            
            // Idler arm
            translate([-extruder_idler_diameter/2, -extruder_idler_thickness, -stepper_side/2]) cube([extruder_idler_diameter, extruder_idler_thickness, stepper_side/2]);
            
            // Idler shroud
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness - 624_thickness - 1, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+1, h=ideal_thickness-1);
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, 0, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+1, h=ideal_thickness-1);

            // Spring arm
            translate([0, -extruder_idler_thickness, -extruder_idler_diameter/2]) cube([stepper_side - 2*stepper_bolt_padding + extruder_idler_diameter/2, extruder_idler_thickness, extruder_idler_diameter]);
            translate([stepper_side - 2*stepper_bolt_padding, -extruder_idler_thickness/2, -extruder_idler_diameter/2 - 1]) cylinder(d=extruder_idler_spring_int_diameter, h=1);
        }

        // Bearing
        translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness + 1, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_ext_diameter+2, h=624_thickness+2);
        translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, extruder_idler_thickness, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) cylinder(d=624_int_diameter, h=3*extruder_idler_thickness);

        // Holes
        translate([0, extruder_idler_thickness, 0]) rotate([90, 0, 0]) cylinder(d=stepper_bolt_diameter, h=3*extruder_idler_thickness);
        translate([stepper_side/2 - 4.8 - stepper_bolt_padding, -extruder_idler_thickness/2, -extruder_idler_diameter]) cylinder(d=4, h=2*extruder_idler_diameter);
    }
}

module extruder() {
    translate([0, -z_padding - x_padding - x_smooth_rod - 0.5, -x_rod_gap/2 - x_smooth_rod]) {
        // Body
        extruder_body();

        // Stepper
        translate([4.8, stepper_depth/2, x_rod_gap + 2*x_smooth_rod + stepper_side/2]) rotate([90, 0, 0]) extruder_stepper();

        // Filament
        translate([0, -16, 20]) cylinder(d=3, h=100);

        // Idler
        translate([-stepper_side/2 + 4.8 + stepper_bolt_padding, -16 + ideal_thickness + 624_thickness/2, x_rod_gap + 2*x_smooth_rod + stepper_side - stepper_bolt_padding]) {
            extruder_idler();
            
            // Idler bearing
            translate([stepper_side/2 - stepper_bolt_padding - 624_ext_diameter/2 - 6.3, -ideal_thickness, -stepper_side/2 + stepper_bolt_padding]) rotate([90, 0, 0]) bearing_624();
        }

    }
}

rotate([180, 0, 0]) extruder_idler();