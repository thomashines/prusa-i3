include <../configuration.scad>
include <../inc/functions.scad>

include <../extruder.scad>

rotate([180, 0, 0]) extruder_idler();