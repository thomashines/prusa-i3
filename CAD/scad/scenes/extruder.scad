include <../configuration.scad>
include <../inc/functions.scad>

include <../extruder.scad>

rotate([-90, 0, 0]) extruder_body();
translate([-70, 80, extruder_idler_diameter/2]) rotate([180, 0, -90]) extruder_idler();