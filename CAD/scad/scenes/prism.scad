include <../configuration.scad>
include <../inc/functions.scad>


right_angle_prism(10, 20, 30);

/*
translate([50, 0, 0]) scale([10, 20, 30]) polyhedron(
    points = [[0, 0, 0], [0, 0, 1], [1, 0, 0], // Front 3 points
              [0, 1, 0], [0, 1, 1], [1, 1, 0]], // Rear 3 points
    faces = [[0, 1, 2], [4, 3, 5], // Triangle ends (front and rear)
             [0, 2, 3], [5, 3, 2], // Horizontal side (z = 0)
             [1, 0, 3], [3, 4, 1], // Vertical side (x = 0)
             [2, 1, 4], [4, 5, 2]] // Hypotenuse side
);
*/

/*
points = [[0, 0, 0], [0, 0, 1], [1, 0, 0], // Front 3 points
          [0, 1, 0], [0, 1, 1], [1, 1, 0]]; // Rear 3 points

faces = [[0, 1, 2], [4, 3, 5], // Triangle ends (front and rear)
         [0, 2, 3], [5, 3, 2], // Horizontal side (z = 0)
         [1, 0, 3], [3, 4, 1], // Vertical side (x = 0)
         [2, 1, 4], [4, 5, 2]]; // Hypotenuse side
*/

/*
translate([-2, -2, -2]) {
    for(i = [0 : 5]) {
        translate(4*points[i]) sphere([1, 1, 1]);
    }
}
*/

/*
face = 0;
translate([-4, -4, -4]) {
    for(i = [0 : 2]) {
        echo(i);
        translate(8*points[faces[face][i]]) sphere([1, 1, 1]);
    }
}
*/

/*
face = [4, 5, 2];
translate([-4, -4, -4]) {
    for(i = [0 : 2]) {
        echo(i);
        translate(8*points[face[i]]) sphere([1, 1, 1]);
    }
}
*/