include <../configuration.scad>
include <../inc/functions.scad>

include <../x-axis.scad>

rotate([0, -90, 0]) x_axis_end_bearing_brace();