include <../configuration.scad>
include <../inc/functions.scad>

include <../x-axis.scad>

rotate([0, 180, 0]) x_axis_idler_mount();