include <../configuration.scad>
include <../inc/functions.scad>

include <../z-axis.scad>

$fn = 64;

/*translate([0, 0, z_smooth_rod_brace_x + ideal_thickness])
rotate([0, -90, 0])
z_smooth_rod_mount();*/
translate([-3 * m3_buffer, 0, ideal_thickness])
rotate([0, -90, 0])
z_servo_mount();
