include <../configuration.scad>
include <../inc/functions.scad>

include <../z-axis.scad>

rotate([0, -90, 0])
z_idler();
translate([10, 0, 0])
mirror([1, 0, 0])
rotate([0, -90, 0])
z_idler();
/*translate([-10, 0, z_smooth_rod_brace_x + ideal_thickness])
rotate([0, -90, 0])
z_smooth_rod_mount();*/
