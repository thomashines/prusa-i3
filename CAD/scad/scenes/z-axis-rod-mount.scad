include <../configuration.scad>
include <../inc/functions.scad>

include <../z-axis.scad>

translate([0, 0, z_smooth_rod_brace_x + ideal_thickness]) rotate([0, -90, 0]) z_smooth_rod_mount();