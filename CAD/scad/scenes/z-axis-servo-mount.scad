include <../configuration.scad>
include <../inc/functions.scad>

include <../z-axis.scad>

translate([stepper_side, 0, 2*m3_buffer]) rotate([0, 180, 0]) z_servo_mount();