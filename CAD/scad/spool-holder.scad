// Rod style
module spool_mount() {
    module cut() {
        translate([2*spool_rod_diameter, -5, 2.5*spool_rod_diameter]) rotate([0, 180, 0]) right_angle_prism(2*spool_rod_diameter, 20, 3*spool_rod_diameter);
    }
    difference() {
        translate([-1.5*spool_rod_diameter, 0, 0]) cube([3*spool_rod_diameter, 10, 2*spool_rod_diameter]);
        cut();
        //mirror([1, 0, 0]) cut();
    }
}

module spool_holder_rod() {
    // Rod
    translate([0, spool_board_length - spool_rod_length, spool_rod_diameter/2]) rotate([-90, 0, 0]) rod(spool_rod_diameter, spool_rod_length);

    // Mounts
    spool_mount();
}

// Board style
module spool_holder_board() {
    // Board
    board(spool_board_width, spool_board_length);
}