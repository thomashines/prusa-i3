module x_smooth() {
    color("LightGrey") cylinder(h = x_length, d = x_smooth_rod);
}

module x_axis_carriage() {
    color("Red") {
        // Body
        difference() {
            cube([x_carriage_length, 2*x_smooth_rod, x_rod_gap + 2*x_smooth_rod]);
            translate([x_carriage_hole_diameter - 0.5, 3*x_smooth_rod, x_carriage_hole_height]) {
                rotate([90, 0, 0]) rod(x_carriage_hole_diameter, 4*x_smooth_rod);
                translate([x_carriage_hole_spacing_1, 0, 0]) {
                    rotate([90, 0, 0]) rod(x_carriage_hole_diameter, 4*x_smooth_rod);
                    translate([x_carriage_hole_spacing_2, 0, 0]) {
                        rotate([90, 0, 0]) rod(x_carriage_hole_diameter, 4*x_smooth_rod);
                        translate([x_carriage_hole_spacing_1, 0, 0]) {
                            rotate([90, 0, 0]) rod(x_carriage_hole_diameter, 4*x_smooth_rod);
                        }
                    }
                }
            }
        }
    }
}

module x_axis_end_bearing_brace() {
    color("Red") translate([0, 0, -m3_buffer]) difference() {
        union() {
            translate([-lm8uu_ext_diameter/2 - m3_buffer, -lm8uu_ext_diameter/2 - 2*m3_buffer, 0]) cube([lm8uu_ext_diameter/2 + m3_buffer - 1, lm8uu_ext_diameter + 4*m3_buffer, m3_buffer*2]);
        }
        translate([0, 0, -1]) cylinder(d=lm8uu_ext_diameter + 0.5, h=2*m3_buffer + 2);
        translate([-lm8uu_ext_diameter/2 - m3_buffer - 1, -lm8uu_ext_diameter/2 - 0.5 - m3_buffer, m3_buffer]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=lm8uu_ext_diameter/2 + m3_buffer + 1);
        translate([-lm8uu_ext_diameter/2 - m3_buffer - 1, +lm8uu_ext_diameter/2 + 0.5 + m3_buffer, m3_buffer]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=lm8uu_ext_diameter/2 + m3_buffer + 1);
    }
}

module x_axis_end_front_cutouts() {
    // Rod fasteners
    translate([-board_thickness + 3*stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x - m3_diameter/2 - m3_buffer, -x_rod_gap_flat/2 + x_smooth_rod/2 + m3_diameter/2 + 2, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + 2);
    translate([-board_thickness + 3*stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x - stepper_side/2, -x_rod_gap_flat/2 + x_smooth_rod/2 + m3_diameter/2 + 2, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + 2);
    translate([-board_thickness + 3*stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x - stepper_side + m3_diameter/2 + m3_buffer, -x_rod_gap_flat/2 + x_smooth_rod/2 + m3_diameter/2 + 2, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + 2);
    // Rod channels
    translate([-1, -x_rod_gap_flat/2, 0]) rotate([0, 90, 0]) cylinder(d=x_smooth_rod + 0.5, h=-x_length/2 + frame_x/2 + board_thickness + m3_buffer + z_smooth_rod + z_threaded_rod + stepper_side + x_stepper_offset_x);
    // Cut off super sharps
    translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer - 1, -x_rod_gap_flat/2 - 1, -x_smooth_rod/2 - 1]) cube([-x_length/2 + frame_x/2 + board_thickness + m3_buffer + z_smooth_rod + z_threaded_rod + stepper_side + x_stepper_offset_x + 2, 4, x_smooth_rod + 2]);
    // Bearing nut cutouts
    translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer + lm8uu_ext_diameter/4 + ideal_thickness/2, -lm8uu_ext_diameter/2 - 0.5 - m3_buffer, -x_smooth_rod/2 + 2 + lm8uu_length/2 + 1]) {
        rotate([90, 0, 90]) m3_nut_cutout(20, 20, 20);
        translate([0, 0, lm8uu_length + 3]) rotate([90, 0, 90]) m3_nut_cutout(20, 20, 20);
    }
    // Prisms
    translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer - 1, -x_rod_gap_flat/2, x_smooth_rod/2 + 1])
    rotate([-90, 0, 0])
    right_angle_prism(45, 10, 15);
}

module x_axis_end() {
    difference() {
        union() {
            // Base
            translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, -x_rod_gap_flat/2, -x_smooth_rod/2]) cube([-x_length/2 + frame_x/2 + board_thickness + m3_buffer + z_smooth_rod + z_threaded_rod + stepper_side + x_stepper_offset_x, x_rod_gap_flat, x_smooth_rod]);
            // Nut brace
            translate([stepper_side/2 - board_thickness, 0, -x_smooth_rod/2]) cylinder(d=z_nut_diameter + 4, h=2*z_nut_depth + z_spring_length + 2, $fn=6);
            // Between bit
            translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, -cos(30)*(z_nut_diameter + 4)/2, -z_smooth_rod/2]) cube([m3_buffer + stepper_side/2, cos(30)*(z_nut_diameter + 4), 2*z_nut_depth + z_spring_length + 2]);
            // Bearing brace
            translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, -lm8uu_ext_diameter/2 - 2*m3_buffer, -x_smooth_rod/2])
            cube([lm8uu_ext_diameter/2 + ideal_thickness, lm8uu_ext_diameter + 4*m3_buffer, 2*(lm8uu_length + 2) + 3*2]);
        }
        // Symmetric part
        x_axis_end_front_cutouts();
        mirror([0, 1, 0]) x_axis_end_front_cutouts();
        // Smooth shaft
        translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, 0, -x_smooth_rod/2 - 1]) cylinder(d=lm8uu_ext_diameter - 2, h=2*(lm8uu_length + 2) + 3*2 + 2);
        // Threaded shaft
        translate([stepper_side/2 - board_thickness, 0, -x_smooth_rod/2 - 1]) cylinder(d=z_threaded_rod + 2, h=2*z_nut_depth + z_spring_length + 2 + 2);
        // Nut
        translate([stepper_side/2 - board_thickness, 0, -x_smooth_rod/2 - 1]) cylinder(d=z_nut_diameter + 0.5, h=2*z_nut_depth + z_spring_length + 1, $fn=6);
        // Bearing
        translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, 0, -x_smooth_rod/2 + 2]) cylinder(d=lm8uu_ext_diameter + 1, h=lm8uu_length + 2);
        translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, 0, -x_smooth_rod/2 + lm8uu_length + 3*2]) cylinder(d=lm8uu_ext_diameter + 1, h=lm8uu_length + 2);
        // Stepper shaft relief
        translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, 0, -x_smooth_rod/2 - 1]) cylinder(d=stepper_shaft_relief_diameter + 1, h=x_smooth_rod + x_stepper_offset_z + 3);
        translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, -(stepper_shaft_relief_diameter + 1)/2, -x_smooth_rod/2 - 1]) cube([stepper_shaft_relief_diameter + 1, stepper_shaft_relief_diameter + 1, x_smooth_rod + x_stepper_offset_z + 4]);
    }
}

module x_axis_drive() {
    color("Red") {
        difference() {
            union() {
                x_axis_end();
                // Stepper offset
                translate([stepper_side/2 - board_thickness + z_threaded_rod/2 + x_stepper_offset_x, -stepper_side/2, 0]) cube([stepper_side, stepper_side, belt_width/2 + stepper_shaft_relief_depth + x_stepper_offset_z]);
            }
            // Stepper mounts
            translate([-board_thickness + stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x + stepper_bolt_padding, stepper_side/2 - stepper_bolt_padding, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + x_stepper_offset_z + 4);
            translate([-board_thickness + 3*stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x - stepper_bolt_padding, stepper_side/2 - stepper_bolt_padding, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + x_stepper_offset_z + 4);
            translate([-board_thickness + stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x + stepper_bolt_padding, -stepper_side/2 + stepper_bolt_padding, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + x_stepper_offset_z + 4);
            translate([-board_thickness + 3*stepper_side/2 + z_threaded_rod/2 + x_stepper_offset_x - stepper_bolt_padding, -stepper_side/2 + stepper_bolt_padding, -x_smooth_rod/2 - 1]) cylinder(d=m3_diameter, h=x_smooth_rod + x_stepper_offset_z + 4);
            // Stepper shaft relief
            translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, 0, -x_smooth_rod/2 - 1]) cylinder(d=stepper_shaft_relief_diameter + 1, h=x_smooth_rod + x_stepper_offset_z + 3);
            translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, -(stepper_shaft_relief_diameter + 1)/2, -x_smooth_rod/2 - 1]) cube([stepper_shaft_relief_diameter + 1, stepper_shaft_relief_diameter + 1, x_smooth_rod + x_stepper_offset_z + 4]);
        }
    }
}

module x_axis_idler_mount() {
    color("Red") difference() {
        union() {
            // Base
            translate([-m3_buffer, -x_rod_gap_flat/2 + x_smooth_rod/2, 2]) cube([2*m3_buffer, x_rod_gap_flat - x_smooth_rod, 2]);
            // Fastener standoffs
            translate([-m3_buffer, -x_rod_gap_flat/2 + x_smooth_rod/2, 0]) cube([2*m3_buffer, 2*m3_buffer, 4]);
            translate([-m3_buffer, +x_rod_gap_flat/2 - x_smooth_rod/2 - 2*m3_buffer, 0]) cube([2*m3_buffer, 2*m3_buffer, 4]);
            // Bearing standoff
            translate([0, 0, 1]) cylinder(d=7, h=3);
        }
        // Fasteners
        translate([0, 0, -1]) cylinder(d=4, h=6);
        translate([0, -x_rod_gap_flat/2 + x_smooth_rod/2 + m3_diameter/2 + 2, -1]) cylinder(d=m3_diameter, h=6);
        translate([0, +x_rod_gap_flat/2 - x_smooth_rod/2 - m3_diameter/2 - 2, -1]) cylinder(d=m3_diameter, h=6);
    }
}

module x_axis_idler() {
    color("Red") {
        difference() {
            union() {
                mirror([1, 0, 0]) x_axis_end();
            }
            // Stepper shaft relief
            translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, 0, -x_smooth_rod/2 - 1]) cylinder(d=stepper_shaft_relief_diameter + 1, h=x_smooth_rod + x_stepper_offset_z + 3);
            translate([stepper_side - board_thickness + x_stepper_offset_x + z_threaded_rod/2, -(stepper_shaft_relief_diameter + 1)/2, -x_smooth_rod/2 - 1]) cube([stepper_shaft_relief_diameter + 1, stepper_shaft_relief_diameter + 1, x_smooth_rod + x_stepper_offset_z + 4]);
        }
    }
}

module x_axis_carriage_front_bearings() {
    translate([0, x_rod_gap_flat/2, 0]) {
        translate([-x_carriage_length/2, 0, 0]) bearing_lm8uu();
        translate([+x_carriage_length/2 - lm8uu_length, 0, 0]) bearing_lm8uu();
    }
}

module x_axis_carriage_front() {
    difference() {
        union() {
            // Bearing outer
            translate([-x_carriage_length/2, x_rod_gap_flat/2, 0]) rotate([0, 90, 0]) cylinder(d=lm8uu_ext_diameter + 12, h=x_carriage_length);
            // Middle
            translate([-x_carriage_length/2, 0, -lm8uu_ext_diameter/2]) cube([x_carriage_length, x_rod_gap_flat/2 - 16, lm8uu_ext_diameter]);
            // Front strut
            translate([-x_carriage_length/2, 0, -lm8uu_ext_diameter/2]) cube([m3_buffer, x_rod_gap_flat/2, lm8uu_ext_diameter]);
            // Rear strut
            translate([+x_carriage_length/2 - m3_buffer, 0, -lm8uu_ext_diameter/2]) cube([m3_buffer, x_rod_gap_flat/2, lm8uu_ext_diameter]);
            // Middle strut
            translate([+x_carriage_length/2 - stepper_depth - 12 - hotend_mount_width/2, 0, -lm8uu_ext_diameter/2]) cube([hotend_mount_width, x_rod_gap_flat/2, lm8uu_ext_diameter]);
        }
        // Bearing inner
        translate([-x_carriage_length/2 - 2, x_rod_gap_flat/2, 0]) rotate([0, 90, 0]) cylinder(d=lm8uu_ext_diameter + 0.5, h=x_carriage_length + 4);
        // Bearing trim
        translate([-x_carriage_length/2 - 1, 0, lm8uu_ext_diameter/2]) cube([x_carriage_length + 2, x_rod_gap_flat/2, lm8uu_ext_diameter]);
        translate([-x_carriage_length/2 - 1, 0, -3*lm8uu_ext_diameter/2]) cube([x_carriage_length + 2, x_rod_gap_flat/2, lm8uu_ext_diameter]);
        // Bearing trim under stepper
        translate([+x_carriage_length/2 - stepper_depth - 12 + hotend_mount_width/2, x_rod_gap_flat/2 - 14, +lm8uu_ext_diameter/2 - 2])
        cube([stepper_depth - hotend_mount_width/2 + m3_buffer + 2, 12, 4]);
        translate([+x_carriage_length/2 - stepper_depth - 12 + hotend_mount_width/2, x_rod_gap_flat/2 + lm8uu_ext_diameter - 4, +lm8uu_ext_diameter]) rotate([0, +90, -90]) right_angle_prism(2*lm8uu_ext_diameter, stepper_depth - hotend_mount_width/2 + m3_buffer + 2, 2*lm8uu_ext_diameter);
        // Bearing prisms
        translate([-x_carriage_length/2 - 1, x_rod_gap_flat/2 + lm8uu_ext_diameter, +lm8uu_ext_diameter]) rotate([0, +90, -90]) right_angle_prism(2*lm8uu_ext_diameter, x_carriage_length + 2, 2*lm8uu_ext_diameter);
        translate([+x_carriage_length/2 + 1, x_rod_gap_flat/2 + lm8uu_ext_diameter, -lm8uu_ext_diameter]) rotate([0, -90, +90]) right_angle_prism(2*lm8uu_ext_diameter, x_carriage_length + 2, 2*lm8uu_ext_diameter);
        // Filament
        translate([+x_carriage_length/2 - stepper_depth - 12, 0, -lm8uu_ext_diameter/2 - 1]) cylinder(d=4, h=lm8uu_ext_diameter + 2);
        // Hotend M3
        translate([+x_carriage_length/2 - stepper_depth - 12, hotend_mount_hole_spacing/2, -lm8uu_ext_diameter/2 - 1]) cylinder(d=m3_diameter, h=lm8uu_ext_diameter + 2);
    }
}

module x_axis_carriage() {
    color("Blue") {
        difference() {
            union() {
                // Symmetric parts
                x_axis_carriage_front();
                mirror([0, 1, 0]) x_axis_carriage_front();
                // Stepper mounts
                translate([x_carriage_length/2 - stepper_depth - m3_buffer, -(filament_gear_diameter + filament_diameter)/2, 0]) {
                    difference() {
                        union() {
                            translate([0, -stepper_side/2, lm8uu_ext_diameter/2]) cube([m3_buffer, 2*stepper_bolt_padding, 2*stepper_bolt_padding]);
                            translate([0, stepper_side/2 - 2*stepper_bolt_padding, lm8uu_ext_diameter/2]) cube([m3_buffer, 2*stepper_bolt_padding, 2*stepper_bolt_padding]);
                        }
                        translate([-1, +stepper_side/2 - stepper_bolt_padding, lm8uu_ext_diameter/2 + stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=stepper_bolt_diameter, h=m3_buffer + 2);
                        translate([-1, -stepper_side/2 + stepper_bolt_padding, lm8uu_ext_diameter/2 + stepper_bolt_padding]) rotate([0, 90, 0]) cylinder(d=stepper_bolt_diameter, h=m3_buffer + 2);
                    }
                }
            }
            // Belt
            // Return
            translate([-x_carriage_length/2 - 1, +belt_gear_diameter/2 - 2, -belt_width/2 - 2]) cube([x_carriage_length + 2, belt_depth + 4, belt_width + 4]);
            // Fixed
            translate([-x_carriage_length/2 - 1, -belt_gear_diameter/2 - belt_depth, -belt_width/2 - 2]) cube([x_carriage_length + 2, belt_depth + 2, belt_width + 4]);
            // Belt fastening
            // Front
            translate([-x_carriage_length/2 + 5, -belt_gear_diameter/2 - belt_depth, -lm8uu_ext_diameter/2 - 1]) cube([10, belt_depth + 2, lm8uu_ext_diameter + 2]);
            translate([-x_carriage_length/2 + 5, -x_rod_gap_flat/2 + lm8uu_ext_diameter/2 + 6, +lm8uu_ext_diameter/2 - 2]) cube([10, 14, 4]);
            // Rear
            translate([+x_carriage_length/2 - 15, -belt_gear_diameter/2 - belt_depth, -lm8uu_ext_diameter/2 - 1]) cube([10, belt_depth + 2, lm8uu_ext_diameter + 2]);
            translate([+x_carriage_length/2 - 15, -x_rod_gap_flat/2 + lm8uu_ext_diameter/2 + 6, +lm8uu_ext_diameter/2 - 2]) cube([10, 14, 4]);
        }
    }
}

module x_axis() {
    // Rods
    // Upper
    translate([-x_length/2, -z_padding - x_padding, +x_rod_gap/2]) rotate([0, 90, 0]) x_smooth();
    // Lower
    translate([-x_length/2, -z_padding - x_padding, -x_rod_gap/2]) rotate([0, 90, 0]) x_smooth();

    // Carriage
    translate([-x_carriage_length/2 + position_x, -z_padding - x_padding - x_smooth_rod, -x_rod_gap/2 - x_smooth_rod]) x_axis_carriage();
}

module x_axis_flat() {
    translate([0, -z_padding, 0]) {
        // Drive end
        translate([-x_length/2, 0, 0]) {
            // Rods
            // Front
            translate([0, +x_rod_gap_flat/2, 0]) rotate([0, 90, 0]) x_smooth();
            // Rear
            translate([0, -x_rod_gap_flat/2, 0]) rotate([0, 90, 0]) x_smooth();
            // Belts
            translate([z_smooth_rod + z_threaded_rod + stepper_side/2 + x_stepper_offset_x, -belt_depth - belt_gear_diameter/2, -belt_width/2]) {
                belt(x_length - 2*(z_smooth_rod + z_threaded_rod + stepper_side/2 + 4));
                translate([0, belt_gear_diameter + belt_depth, 0]) belt(x_length - 2*(z_smooth_rod + z_threaded_rod + stepper_side/2 + 4));
            }
            // Drive
            x_axis_drive();
            translate([x_length/2 - frame_x/2 - board_thickness - m3_buffer, 0, -x_smooth_rod/2 + 2 + 1]) {
                // Bearings
                rotate([0, -90, 0]) bearing_lm8uu();
                translate([0, 0, lm8uu_length + 4]) rotate([0, -90, 0]) bearing_lm8uu();
                // Bearing braces
                translate([0, 0, lm8uu_length/2]) x_axis_end_bearing_brace();
                translate([0, 0, 3*lm8uu_length/2 + 3]) x_axis_end_bearing_brace();
            }
            // Stepper
            translate([z_smooth_rod + z_threaded_rod + stepper_side/2 + x_stepper_offset_x, 0, stepper_depth/2 + belt_width/2 + stepper_shaft_relief_depth + x_stepper_offset_z]) rotate([180, 0, 0]) stepper();
            // Nut trap
            translate([0, 0, -1]) z_nut_trap();
        }
        // Idle end
        translate([x_length/2, 0, 0]) {
            // Idler
            x_axis_idler();
            translate([-x_length/2 + frame_x/2 + board_thickness + m3_buffer, 0, -x_smooth_rod/2 + 2 + 1]) {
                // Bearings
                rotate([0, -90, 0]) bearing_lm8uu();
                translate([0, 0, lm8uu_length + 4]) rotate([0, -90, 0]) bearing_lm8uu();
                // Bearing braces
                translate([0, 0, lm8uu_length/2]) rotate([0, 0, 180]) x_axis_end_bearing_brace();
                translate([0, 0, 3*lm8uu_length/2 + 3]) rotate([0, 0, 180]) x_axis_end_bearing_brace();
            }
            // Mounts for the idler bearing
            translate([-z_smooth_rod - z_threaded_rod - stepper_side/2 - x_stepper_offset_x, 0, x_smooth_rod/2]) x_axis_idler_mount();
            translate([-z_smooth_rod - z_threaded_rod - stepper_side/2 - x_stepper_offset_x, 0, -x_smooth_rod/2]) rotate([180, 0, 0]) x_axis_idler_mount();
            // Nut trap
            translate([0, 0, -1]) mirror([1, 0, 0]) z_nut_trap();
        }
        // Carriage
        translate([position_x - x_carriage_length/2 + stepper_depth + filament_stepper_gap, 0, 0]) {
            // Carriage
            x_axis_carriage();
            // Stepper
            translate([x_carriage_length/2 - stepper_depth/2, -(filament_gear_diameter + filament_diameter)/2, lm8uu_ext_diameter/2 + stepper_side/2]) rotate([0, -90, 0]) stepper();
            // Bearings
            x_axis_carriage_front_bearings();
            mirror([0, 1, 0]) x_axis_carriage_front_bearings();
        }
    }
}
