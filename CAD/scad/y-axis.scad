module y_smooth(length) {
    color("LightGrey") cylinder(h = length, d = y_smooth_rod);
}

module y_threaded(length) {
    color("LightGrey") cylinder(h = length, d = y_smooth_rod);
}

module y_washer() {
    washer(y_threaded_rod + 1, y_washer_external_diameter, y_washer_thickness);
}

module y_nut() {
    nut(y_threaded_rod + 1, y_nut_external_diameter, y_nut_thickness);
}

module y_axis_corner() {
    color("Red") difference() {
        // Body
        cube([3*y_threaded_rod, 3*y_threaded_rod, 6*y_threaded_rod]);
        // First cut, x, threaded
        translate([-0.5*y_threaded_rod, 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) rod(y_threaded_rod + 1, 4*y_threaded_rod);
        // Second cut, y, threaded
        translate([1.5*y_threaded_rod, 3.5*y_threaded_rod, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) rod(y_threaded_rod + 1, 4*y_threaded_rod);
        // Third cut, x, threaded
        translate([-0.5*y_threaded_rod, 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) rod(y_threaded_rod + 1, 4*y_threaded_rod);
        // Fourth cut, y, smooth
        translate([1.5*y_threaded_rod, 2.5*y_threaded_rod, 6*y_threaded_rod - y_smooth_rod/4]) rotate([90, 0, 0]) rod(y_smooth_rod + 1, 4*y_threaded_rod);
    }
}

module assembled_corner() {
    // Front right
    
    // Block
    translate([y_width/2 + 1.5*y_threaded_rod, -y_length/2 + y_difference + 3*y_threaded_rod, 0]) rotate([0, 0, 180]) y_axis_corner();
    
    // Washers
    // Upper
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_washer();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_washer();
    // Lower
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_washer();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_washer();
    // Paralell
    translate([y_width/2, -y_length/2 + y_difference - y_washer_thickness/2 - 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_washer();
    translate([y_width/2, -y_length/2 + y_difference + 3*y_threaded_rod + y_washer_thickness/2 + 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_washer();

    // Nuts
    // Upper
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness + 0.5 + y_nut_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness - 0.5 - y_nut_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness - 0.5 - y_nut_thickness - 0.5 - y_nut_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) y_nut();
    // Lower
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 + 1.5*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness + 0.5 + y_nut_thickness/2 + 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness - 0.5 - y_nut_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_nut();
    translate([y_width/2 - 1.5*y_threaded_rod - y_washer_thickness - 0.5 - y_nut_thickness - 0.5 - y_nut_thickness/2 - 0.5, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) y_nut();
    // Paralell
    translate([y_width/2, -y_length/2 + y_difference - y_washer_thickness - 0.5 - y_nut_thickness/2 - 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_nut();
    translate([y_width/2, -y_length/2 + y_difference - y_washer_thickness - 0.5 - y_nut_thickness - 0.5 - y_nut_thickness/2 - 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_nut();
    translate([y_width/2, -y_length/2 + y_difference + 3*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness/2 + 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_nut();
    translate([y_width/2, -y_length/2 + y_difference + 3*y_threaded_rod + y_washer_thickness + 0.5 + y_nut_thickness + 0.5 + y_nut_thickness/2 + 0.5, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) y_nut();
}

module assembled_rod_across() {
    // Front upper
    translate([-y_width/2 - y_difference - 1.5*y_threaded_rod, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4 + 20]) rotate([0, 90, 0]) rod(y_threaded_rod, y_width + 2*y_difference + 3*y_threaded_rod);
    // Front lower
    translate([-y_width/2 - y_difference - 1.5*y_threaded_rod, -y_length/2 + y_difference + 1.5*y_threaded_rod, frame_bottom_height/2 + y_threaded_rod/4]) rotate([0, 90, 0]) rod(y_threaded_rod, y_width + 2*y_difference + 3*y_threaded_rod);
}

module assembled_rod_parallel() {
    // Right
    // Threaded
    translate([y_width/2, y_length/2, frame_bottom_height + y_threaded_rod/2]) rotate([90, 0, 0]) rod(y_threaded_rod, y_length);
    // Smooth
    translate([y_width/2, y_length/2 - y_difference - 0.5*y_threaded_rod - 0.5, 6*y_threaded_rod - y_smooth_rod/4]) rotate([90, 0, 0]) rod(y_smooth_rod, y_length - 2*y_difference - y_threaded_rod - 1);
}

module y_axis() {
    // Corners, washers and nuts
    // Front right
    assembled_corner();
    // Front left
    mirror([1, 0, 0]) assembled_corner();
    // Rear right
    mirror([0, 1, 0]) assembled_corner();
    // Rear left
    mirror([1, 0, 0]) mirror([0, 1, 0]) assembled_corner();

    // Rods
    // Across
    // Front
    assembled_rod_across();
    // Rear
    mirror([0, 1, 0]) assembled_rod_across();

    // Paralell
    // Right
    assembled_rod_parallel();
    // Left
    mirror([1, 0, 0]) assembled_rod_parallel();
}
