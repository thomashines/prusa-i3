module z_smooth(length) {
    color("LightGrey") cylinder(h = length, d = z_smooth_rod);
}

module z_threaded(length) {
    color("LightGrey") cylinder(h = length, d = z_threaded_rod);
}

module z_nut_trap() {
    color("Blue") {
        translate([stepper_side/2 - board_thickness,
                                                  0,
                   -x_smooth_rod/2 - z_leadnut_depth]) {
            difference() {
                union() {
                    cylinder(d=z_leadnut_diameter + 2*ideal_thickness, h=z_leadnut_depth);
                    translate([-30, -z_smooth_rod/2 - ideal_thickness, 0])
                    cube([30, z_smooth_rod + 2*ideal_thickness, z_leadnut_depth]);
                }
                translate([0, 0, -1])
                cylinder(d=z_leadnut_diameter + 0.25, h=z_leadnut_depth + 2);
                translate([-31, -z_smooth_rod/2, -1])
                cube([z_smooth_rod + 2, z_smooth_rod, z_leadnut_depth + 2]);
            }
        }
    }
}

module z_shaft_couple() {
    color("Green") {
        difference() {
            // Body
            cylinder(d=z_shaft_couple_d, h=z_shaft_couple_h);

            // Stepper shaft
            translate([0, 0, -1])
            difference() {
                cylinder(d=stepper_shaft_diameter + 0.5, h=z_shaft_couple_h + 2);
                translate([-stepper_shaft_diameter / 2 + 4.75, -stepper_shaft_diameter / 2, 0])
                cube([stepper_shaft_diameter, stepper_shaft_diameter, stepper_shaft_length + 2]);
            }

            // Lead rod
            translate([0, 0, z_shaft_couple_h - 16])
            /*metric_thread(z_threaded_rod, 2, 17, internal=true, n_starts=1);*/
            cylinder(d=z_threaded_rod + 0.25, h=17);

            // Stepper shaft set screw
            translate([z_shaft_couple_d / 2 - 5, 0, stepper_shaft_length / 2])
            rotate([180, 0, 0])
            rotate([0, 90, 0])
            m3_nut_cutout(20, 10, 20);

            // Lead rod set screw
            translate([z_shaft_couple_d / 2 - 3.5, 0, z_shaft_couple_h - 8])
            rotate([0, 90, 0])
            m3_nut_cutout(20, 10, 20);
        }
    }
}

module z_servo_mount() {
    //union() {
    difference() {
        union() {
            // Bottom
            cube([stepper_side, stepper_side/2 + z_padding, 2*m3_buffer]);

            // Top
            translate([0, 0, z_driven_z - ideal_thickness])
            cube([stepper_side, stepper_side/2 + z_padding, ideal_thickness]);

            // Back
            translate([0, stepper_side/2 + z_padding - ideal_thickness, 0])
            cube([stepper_side, ideal_thickness, z_driven_z]);

            // Side
            translate([-ideal_thickness, 0, 0])
            cube([ideal_thickness, z_padding + stepper_side/2 + board_thickness/2 + woodscrew_buffer, z_driven_z]);
        }

        // Stepper relief cutout
        translate([stepper_side/2, stepper_side/2, z_driven_z - ideal_thickness - 1])
        cylinder(d=stepper_shaft_relief_diameter + 2, h=ideal_thickness + 2);
        translate([stepper_side/2 - stepper_shaft_relief_diameter/2 - 1, -1, z_driven_z - ideal_thickness - 1])
        cube([stepper_shaft_relief_diameter + 2, stepper_side/2 + 1, ideal_thickness + 2]);

        // Threaded rod cutout
        translate([stepper_side/2, stepper_side/2, -1])
        cylinder(d=z_threaded_rod + 2, h=2 * m3_buffer + 2);

        // Smooth rod cutout
        translate([-m3_buffer, stepper_side/2, -1])
        cylinder(d=z_smooth_rod + 0.5, h=z_driven_z + 2);

        // Smooth rod bolt cutouts
        translate([0, 1*stepper_side/4 + 1.5, m3_buffer])
        rotate([180, 0, 0])
        rotate([0, 90, 0])
        m3_nut_cutout(20, 50, 10);
        translate([0, 3*stepper_side/4 - 1.5, m3_buffer])
        rotate([180, 0, 0])
        rotate([0, 90, 0])
        m3_nut_cutout(20, 50, 10);

        // Stepper bolt cutouts
        translate([stepper_bolt_padding, stepper_bolt_padding, 0])
        mirror([0, 0, 1])
        scale([1, 1, 3])
        m3_bolt_cutout();
        translate([stepper_side - stepper_bolt_padding, stepper_bolt_padding, 0])
        mirror([0, 0, 1])
        scale([1, 1, 3])
        m3_bolt_cutout();
        translate([stepper_bolt_padding, stepper_side - stepper_bolt_padding, 0])
        mirror([0, 0, 1])
        scale([1, 1, 3])
        m3_bolt_cutout();
        translate([stepper_side - stepper_bolt_padding, stepper_side - stepper_bolt_padding, 0])
        mirror([0, 0, 1])
        scale([1, 1, 3])
        m3_bolt_cutout();

        // Woodscrew cutouts
        // Side
        translate([-ideal_thickness, z_padding + stepper_side/2 + board_thickness/2, m3_buffer])
        rotate([0, 90, 0])
        woodscrew_cutout();
        translate([-ideal_thickness, z_padding + stepper_side/2 + board_thickness/2, z_driven_z - m3_buffer])
        rotate([0, 90, 0])
        woodscrew_cutout();
        // Back
        translate([m3_buffer + board_thickness, stepper_side/2 + z_padding - ideal_thickness, z_driven_z / 2])
        rotate([-90, 0, 0])
        woodscrew_cutout();
        translate([stepper_side - m3_buffer, stepper_side/2 + z_padding - ideal_thickness, z_driven_z / 2])
        rotate([-90, 0, 0])
        woodscrew_cutout();
    }
}

module z_idler() {
    //union() {
    difference() {
        union() {
            translate([0, -z_smooth_rod/2 - 2*m3_buffer, 0]) cube([stepper_side, z_padding + z_smooth_rod/2 + 2*m3_buffer, 2*m3_buffer]); // Top
            translate([0, z_padding - ideal_thickness, 2*m3_buffer]) cube([stepper_side, ideal_thickness, z_idler_height]); // Below frame standoff
            translate([-ideal_thickness, -z_smooth_rod/2 - 2*m3_buffer, 0]) cube([ideal_thickness, z_padding + z_smooth_rod/2 + 2*m3_buffer + board_thickness/2 + woodscrew_buffer, z_idler_height + 2*m3_buffer]); // Side
        }
        // Woodscrew cutouts
        translate([board_thickness/2, z_padding - ideal_thickness, ideal_thickness + 2*woodscrew_buffer]) rotate([-90, 0, 0]) woodscrew_cutout();
        translate([stepper_side - board_thickness/2, z_padding - ideal_thickness, ideal_thickness + 2*woodscrew_buffer]) rotate([-90, 0, 0]) woodscrew_cutout();
        translate([-ideal_thickness, z_padding + board_thickness/2, m3_buffer]) rotate([0, 90, 0]) woodscrew_cutout();
        translate([-ideal_thickness, z_padding + board_thickness/2, z_idler_height + m3_buffer]) rotate([0, 90, 0]) woodscrew_cutout();
        // Prism cutout
        translate([stepper_side + 1, -z_smooth_rod/2 - 2*m3_buffer - 1, 2*m3_buffer + z_idler_height + 1]) rotate([0, 90, 90]) right_angle_prism(z_idler_height + 1, stepper_side + ideal_thickness + 2, z_padding + z_smooth_rod/2 + 2*m3_buffer - ideal_thickness + 1);
        // Smooth rod cutout
        translate([-m3_buffer, 0, -1]) cylinder(d=z_smooth_rod + 0.5, h=z_idler_height + 2*m3_buffer + 2);
        // Smooth rod bolt cutouts
        translate([m3_buffer, +stepper_side/4 - 1.5, m3_buffer]) rotate([0, 90, 0]) m3_nut_cutout(20, 40, 10);
        translate([m3_buffer, -stepper_side/4 + 1.5, m3_buffer]) rotate([0, 90, 0]) m3_nut_cutout(20, 40, 10);
        // Bearing cutout
        // Upper
        /*translate([stepper_side/2, 0, m3_buffer])
        cylinder(d=608_ext_diameter + 0.5, h=m3_buffer + 1);*/
        /*translate([stepper_side/2 - (608_ext_diameter + 0.5)/2, -z_smooth_rod/2 - 2*m3_buffer - 1, m3_buffer])
        cube([608_ext_diameter + 0.5, z_smooth_rod/2 + 2*m3_buffer + 1, m3_buffer + 1]);*/
        // Lower
        translate([stepper_side/2, 0, -1])
        cylinder(d=z_threaded_rod + 2, h=2*m3_buffer + 2);
        /*translate([stepper_side/2 - (608_int_diameter + 2)/2, -z_smooth_rod/2 - 2*m3_buffer - 1, -1])
        cube([608_int_diameter + 2, z_smooth_rod/2 + 2*m3_buffer + 1, 2*m3_buffer + 2]);*/
    }
}

module z_smooth_rod_mount() {
    //union() {
    difference() {
        translate([-z_smooth_rod_brace_x - ideal_thickness, stepper_side/8, 0]) cube([z_smooth_rod_brace_x, 3*stepper_side/4, 2*m3_buffer]);
        // Smooth rod cutout
        translate([-m3_buffer + 1, stepper_side/2, -z_rod_start_height - 1]) rotate([0, 0, 0]) cylinder(d=z_smooth_rod + 0.5, h=z_rod_start_height + 2*m3_buffer + 2);
        // Smooth rod bolt cutouts
        translate([-3*z_smooth_rod/2 - ideal_thickness, stepper_side/4 + 1.5, m3_buffer]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=2*z_smooth_rod);
        translate([-3*z_smooth_rod/2 - ideal_thickness, 3*stepper_side/4 - 1.5, m3_buffer]) rotate([0, 90, 0]) cylinder(d=m3_diameter, h=2*z_smooth_rod);
    }
}

module z_left() {
    translate(
        [-frame_x/2 - board_thickness,
         -z_padding,
         -frame_z/2 + z_rod_start_height]) {

        // Smooth rod
        translate([-m3_buffer, 0, -2 * m3_buffer])
        z_smooth(z_smooth_rod_h);

        // Idler
        color("white")
        translate([0, 0, 0])
        mirror([0, 0, 1])
        z_idler();

        // Idler smooth rod mount
        color("white")
        translate([-1, -stepper_side/2, -2 * m3_buffer])
        z_smooth_rod_mount();

        // Driven end
        translate([0, 0, z_length - 2 * m3_buffer - 9]) {
            // Stepper mount
            color("white") translate([0, -stepper_side/2, 0]) z_servo_mount();

            // Driven smooth rod mount
            color("white") translate([-1, -stepper_side/2, 0]) z_smooth_rod_mount();

            // Threaded rod
            translate([stepper_side/2, 0, 2 * m3_buffer + 9])
            rotate([180, 0, 0])
            z_threaded(z_threaded_rod_h);

            // Stepper
            translate([stepper_side/2, 0, z_driven_z + stepper_depth / 2])
            rotate([180, 0, 0])
            stepper();
        }

    }
}

module z_axis() {
    // Left
    z_left();
    // Right
    mirror([1, 0, 0]) z_left();

}
