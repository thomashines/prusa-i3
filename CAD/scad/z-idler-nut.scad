include <inc/threads.scad>

difference() {
    union() {
        cylinder(d=8.3, h=10.0, $fn=32);
        cylinder(d=12.0, h=2.0, $fn=32);
    }
    //translate([0, 0, 11]) rotate([180, 0, 0])    metric_thread(6.1, 1, 12, true);
    translate([0, 0, -1]) cylinder(d=5.5, h=12, $fn=32);
}