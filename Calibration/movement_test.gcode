G21 ; set units to millimeters
M107 ; Fan off
G28 X0 Y0 ; home X and Y

G1 F600 ; Set feedrate

G1 X45 Y45 ; Top left

M84 ; Disable motors